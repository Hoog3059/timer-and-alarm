﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.ComponentModel;
using System.IO;
using MahApps.Metro.Controls;

namespace Timer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        //variables
        public string setTime;
        public bool enabled = false;
        public bool forcingShut = false;
        private MediaPlayer clockSound = new MediaPlayer();
        System.Windows.Forms.NotifyIcon nIcon = new System.Windows.Forms.NotifyIcon();
        public bool blnTip = true;
        
        public MainWindow()
        {
            InitializeComponent();            
        }
        
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            //window
            this.WindowState = System.Windows.WindowState.Minimized;
            this.ShowInTaskbar = false;
                                    
            //notify icon
            nIcon.Icon = new Icon("References\\icon.ico");
            nIcon.Visible = true;
            nIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(nIcon_MouseClick);
            nIcon.ShowBalloonTip(500, "Timer Is Running", "hit ME to show timer", System.Windows.Forms.ToolTipIcon.Info);
            
            //Closing Event
            System.Windows.Application.Current.MainWindow.Closing += new CancelEventHandler(MainWindow_Closing);

            //dispatcher
            System.Windows.Threading.DispatcherTimer Timer = new System.Windows.Threading.DispatcherTimer();
            Timer = new System.Windows.Threading.DispatcherTimer();
            Timer.Tick += new EventHandler(Timer_Tick);
                             //TimeSpan(hours, min, sec);
            Timer.Interval = new TimeSpan(0, 0, 5);
            Timer.Start();

            //mediaplayer
            if(File.Exists("References\\alarm.mp3"))
            {
                
            }
            else
            {
                MessageBox.Show("ERROR: Can't find file 'References\\alarm.wav', without this file Timer while possible not work", "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void nIcon_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Normal;
            this.ShowInTaskbar = true;
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (forcingShut)
            {

            }
            else
            {
                this.WindowState = System.Windows.WindowState.Minimized;
                this.ShowInTaskbar = false;
                if (blnTip)
                {
                    nIcon.ShowBalloonTip(500, "I'm running!", "To stop me, open me and hit EXIT", System.Windows.Forms.ToolTipIcon.Info);
                }                
                e.Cancel = true;
            }            
        }

        private void Button_Shutdown_Click(object sender, RoutedEventArgs e)
        {
            forcingShut = true;
            System.Windows.Application.Current.Shutdown();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (enabled)
            {
                string temp = DateTime.Now.ToString("HH:mm");
                if (temp == setTime)
                {
                    this.WindowState = System.Windows.WindowState.Normal;
                    clockSound.Open(new Uri(Directory.GetCurrentDirectory() + "\\References\\alarm.mp3", UriKind.Absolute));
                    clockSound.Play();                    
                    MessageBox.Show("Timer!!!", "timer", MessageBoxButton.OK, MessageBoxImage.Warning);
                    nIcon.ShowBalloonTip(500, "Timer!!!!!", "Your timer goes off", System.Windows.Forms.ToolTipIcon.Info);
                    clockSound.Close();
                    enabled = false;
                    lblTimeSet.Content = "none";
                    this.WindowState = System.Windows.WindowState.Minimized;
                }
            }
            else
            {
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (picker.Value.Value.ToString() != "")
            {
                string temp;
                if (picker.Value.Value.Hour < 10)
                {
                    temp = "";
                    temp = "0" + picker.Value.Value.Hour.ToString();
                }
                else
                {
                    temp = "";
                    temp = picker.Value.Value.Hour.ToString();
                }
                
                if (picker.Value.Value.Minute < 10)
                {
                    temp = temp + ":0" + picker.Value.Value.Minute.ToString();
                }
                else
                {
                    temp = temp + ":" + picker.Value.Value.Minute.ToString();
                }
                
                setTime = temp;
                MessageBox.Show("Timer set for " + setTime);
                enabled = true;
                lblTimeSet.Content = setTime;
            }           
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if ((MessageBox.Show("Are you sure to stop the timer", "Stop", MessageBoxButton.YesNo, MessageBoxImage.Question)) == MessageBoxResult.Yes)
            {
                enabled = false;
                lblTimeSet.Content = "none";
                setTime = "";
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SettingWindow sw = new SettingWindow();
            sw.ShowDialog();
        }
    }
}
